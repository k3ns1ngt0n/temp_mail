import requests
import random
import string
import time
import os
from sys import exit

API = 'https://www.1secmail.com/api/v1/'
domain_list = ["1secmail.com", "1secmail.org", "1secmail.net"]
domain = random.choice(domain_list)


def generate_username():
    name = string.ascii_lowercase + string.digits
    username = ''.join(random.choice(name) for i in range(10))

    return username


def check_mail(mail=''):
    req_link = f'{API}?action=getMessages&login={mail.split("@")[0]}&domain={mail.split("@")[1]}'
    r = requests.get(req_link).json()
    length = len(r)

    if length == 0:
        print('[INFO] There is no new incoming messages. Status updates every 5 seconds')
    else:
        id_list = []

        for i in r:
            for k,v in i.items():
                if k == 'id':
                    id_list.append(v)

        print(f'[+] You have {length} unread messages\n\n')

        for i in id_list:
            read_msg = f'{API}?action=readMessage&login={mail.split("@")[0]}&domain={mail.split("@")[1]}&id={i}'
            r = requests.get(read_msg).json()

            sender = r.get('from')
            subject = r.get('subject')
            date = r.get('date')
            content = r.get('textBody')
            
            print(f'[INFO] {sender} [{date}]:\n{content}\n\n')

        print(f'[!] Mailbox {mail} will be deleted in 10 seconds...\n')
        time.sleep(10)
        delete_mail(mail=mail)
        exit(0)


def delete_mail(mail=''):
    url = 'https://www.1secmail.com/mailbox'

    data = {
        'action': 'deleteMailbox',
        'login': mail.split('@')[0],
        'domain': mail.split('@')[1]
    }

    r = requests.post(url, data=data)
    print(f'[X] Mailbox {mail} - deleted\n')


def main():
    try:
        username = generate_username()
        mail = f'{username}@{domain}'
        print(f'[+] Your mailbox: {mail}')

        mail_req = requests.get(f'{API}?login={mail.split("@")[0]}&domain={mail.split("@")[1]}')

        while True:
            check_mail(mail=mail)
            time.sleep(5)

    except(KeyboardInterrupt):
        delete_mail(mail=mail)
        print('[!] Manually interrupted')


if __name__ == '__main__':
    main()
